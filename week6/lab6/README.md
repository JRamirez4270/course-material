# Lab 6 - Assemble UAS Frame (15 pts.)

[TOC]

---

## 1. Objectives

The objective of this lab is to use many of the same principles from [Lab 3](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/deede154c09dd7358b3efe60e2cdf4029a8745da/week3/lab3/README.md), but in reverse. The ultimate objective it to create an airworthy unmanned aircraft system (UAS) capable of stable flight with an attached payload. We will accomplish this through careful construction and meticulous documentation of process and procedures. 

We will develop the skills to assemble the UAS frame components for phase one while planning for the integration of electronic components in the next phases.

---

## 2. Overview

The airworthiness requirements for UAS are outlined in [Title 14 § 107.15 Condition for safe operation (a)](https://www.ecfr.gov/current/title-14/chapter-I/subchapter-F/part-107#p-107.15(a)) which states: 

> No person may operate a civil small unmanned aircraft system unless it is in a condition for safe operation. Prior to each flight, the remote pilot in command must check the small unmanned aircraft system to determine whether it is in a condition for safe operation. (14 CFR 107.15(a))

That is, while the UAS is not required to comply with airworthiness standards, it is fully the responsibility of the operator of the aircraft to perform the required preflight inspection. ([AC 107](https://www.faa.gov/documentLibrary/media/Advisory_Circular/AC_107-2A.pdf#%5B%7B%22num%22%3A79%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C70%2C434%2C0%5D)) The operator will be best prepared if they maintain up-to-date and complete records. 

The FAA recommends following the manufacturer's maintenance instructions/schedule. However, we are building our UAS from scratch meaning that we are the manufacturer and will need to ensure that a proper maintenance schedule is provided for the end user - even if that end user is ourselves. 

In this lab we will assemble phase one of out UAS - the frame. We will also begin creating a maintenance and work performed log and schedule. 

---

## 3. Equipment List

This lab will require the following equipment. Each team is responsible for gathering the required equipment and tools.

- [ ] Quadcopter kit [HobbyKing X650F-V4](https://hobbyking.com/en_us/hobbyking-x650f-glass-fiber-quadcopter-frame-550mm.html)

- [ ] Toolkit

- [ ] Scale (communal item)

- [ ] Computer (one per group)

## 4. Assignment

Each assignment can be completed either within this markdown file and pushed to GitLab or any other utility teams choose and uploaded to Brightspace. 

> :green_book: Please read through the assignments completely before beginning disassembly as the assignments are not necessarily chronological.

### 4.1 Review AC 107-2A

The most recent sUAS Advisory Circular from the FAA contains information and guidance on maintaining and inspecting UAS. Please read [Chapter 7](https://www.faa.gov/documentLibrary/media/Advisory_Circular/AC_107-2A.pdf#%5B%7B%22num%22%3A160%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C70%2C720%2C0%5D) and [Appendix C](https://www.faa.gov/documentLibrary/media/Advisory_Circular/AC_107-2A.pdf#%5B%7B%22num%22%3A375%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C70%2C720%2C0%5D). You will base your maintenance records and logs off of this document. Additionally, any content within Chapter 7 and Appendix C are fair game quiz material.

### 4.2 Tool Report (1 pt.)

Recall from [Lab 3](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/66baec12a3efdad7f624aa6507c6a402c4708ffd/week3/lab3/README.md#41-tool-report-5-pts) the importance of ensuring the every tool is present and accounting for both before and after performing work. The check in and checkout procedure below will be completed during every phase of the build.

#### Toolkit ID

By filling in your toolkit ID, you verify that your have received all of the tools listed in the [toolkit checklist](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/66baec12a3efdad7f624aa6507c6a402c4708ffd/resources/toolkit-checklist.md), and have completed the checklist at both checkout and checkin.

**Toolkit ID:** **`          `**

### 4.3 Inventory UAS Kit (1 pt.)

The first step to building and product is to take an inventory of the parts needed for the build.

1. Carefully unpack the contents of the kit. 
   
   ![lab6_kitContents](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_kitContents.jpg)

2. Watch [this short video](https://www.youtube.com/embed/2762vGCckHs) describing what bill of materials is and why it is important.

3. Complete a multi-level bill of materials (BOM) table for your UAS. This is a living document and can/will be updated in future phases.

### 4.4 Assemble UAS Frame (8 pts.)

Carefully assemble the UAS following the instructions below. Be sure not to overtighten the screws as they could strip or crack the frame.

#### 4.4.1 Assemble the control board

1. Find the main plate pictured below along with the screw bag M3\*25 nylon spacers and M3\*6 screws (6 pcs.) **What material is the main plate made out of?**
   
   ![lab6_controlBoard_1](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_controlBoard_1.png)

2. Assemble the center board using the M3\*25 nylon spacers and M3\*6 screws then add the cover.
   
   ![lab6_controlBoard_2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_controlBoard_2.png)

#### 4.4.2 Assemble the main frames

1. Find the main frame components that were included with center board package.
   
   ![lab6_mainFrame_1](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_mainFrame_1.png)

2. Assemble the upper and bottom plate using M3\*14.9 aluminum hex spacers (8 pcs.) and M3\*6 screws (16 pcs.).
   
   ![lab6_mainFrame_2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_mainFrame_2.png)

#### 4.4.3 Assemble motor mounts and arms

1. Find the fiberglass motor mounts (4 pcs.), aluminum square tubes (4 pcs. total - 2 yellow, 2 black), and remaining hardware.
   
   ![lab6_motorMountArms_1](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_motorMountArms_1.png)

2. Assemble the motor mounts to the arms using the M3\*15 aluminum round spacers (8 pcs.) and M3\*6 screws (16 pcs.)
   
   ![lab6_motorMountArms_2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_motorMountArms_2.png)

#### 4.4.4 Assemble the arms to main frame

1. Assemble the arms to the main frame such that two yellow arms are adjacent to one another. Use the M3\*25 screws (16 pcs.), M3 nuts (16 pcs.), and M3 nylon ring (16 pcs.) to install the arms such that the nylon rings are between the screw and the fiber glass plate. **What is the purpose of the nylon rings?**
   
   ![lab6_armsToFrame_1](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_armsToFrame_1.png)

2. Attach the center board assembly to the main frame board.
   
   ![lab6_armsToFrame_2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_armsToFrame_2.png)

#### 4.4.5 Assemble payload mount bracket

1. Find aluminum eyelets (4 pcs.), rubber grommets (4 pcs.), and M3\*4 screws (8 pcs.)
   
   ![lab6_payloadMount_1](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_payloadMount_1.png)

2. Insert rubber grommets into aluminum eyelets and attach to the bottom plate. Insert the 10\*8\*30 mm fiberglass tubes through the grommets.
   
   ![lab6_payloadMount_2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_payloadMount_2.png)

#### 4.4.6 Assemble battery tray and landing gear

1. Attach plastic battery bracket and battery tray together using M2.5\*8 screws (4 pcs.)
   
   ![lab6_batteryTrayLandingGear_1](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_batteryTrayLandingGear_1.png)

2. Snap the plastic bracket and landing gear onto the 10\*8\*30 mm tubes.
   
   ![lab6_batteryTrayLandingGear_2](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_batteryTrayLandingGear_2.png)

3. Snap 8\*6\*35 mm fiberglass tubes onto the landing gear. 
   
   ![lab6_batteryTrayLandingGear_3](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab6_batteryTrayLandingGear_3.png)

### 4.5 Logs, Schedules, Checklists, and Manuals (5 pts.)

As the manufacturer of your UAS, it is your responsibility to create all of the documentation needed to accompany your UAS from lab to field. 

Begin to think about the infrastructure and outline for your documentation templates. The [UAV Documentation Examples](https://facilities.siu.edu/uav/doc-examples/index.php) website from Southern Illinois University is a great place to begin getting ideas for creating your documentation. Do not recreate the wheel, but do tailor examples to your specific UAS.

Again, groups may choose any platform they wish for maintaining their documents and should discuss option with the instructor before choosing one. Some options include:

1. GitLab Wiki

2. Purdue Confluence Wiki 

3. Microsoft Office

4. A combination of the above

Aim to spend 15-20 minutes setting up your documentation structure. Be sure that the instructor signs off on your documentation outline.



:wave: Hey there! Notice anything you would like improved? Suggestions are always welcomed! Please [create a new issue](https://gitlab.com/purdue-uas/at21900/course-material/-/issues) on GitLab or [email](mailto:incoming+purdue-uas-at21900-course-material-32681911-47fahqsg66ijh2ikfun4yji7d-issue@incoming.gitlab.com?subject=[lab3]) your suggestion. 

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
