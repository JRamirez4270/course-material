# Lab Report Format Guidelines

## Overview

After completing a lab you will write a lab report to summarize your findings. The lab report should help you understand the foundation of technical communication through writing. 
