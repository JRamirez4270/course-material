# Git Etiquette Guidelines

## Overview

This Git Etiquettes, "gitiquettes", is a list of best practices for team working using Git and GitLab. This is an evolving list that will be updated as new observations are made. These suggestions are listed in no particular order or significance. 

1. When working on a team project with one person acting as the scribe, proper gitiquette is to `push` the repository often throughout the class for backup and at the very least once at the end of lab so that others can checkout the lab for themselves.

2. 
