#include <Servo.h>

byte servoPin = 9; // signal pin for the ESC.
byte potentiometerPin = A0; // analog input pin for the potentiometer.
Servo servo;

void setup() {
  Serial.begin(115200);
servo.attach(servoPin);
servo.writeMicroseconds(1500); // send "stop" signal to ESC. Also necessary to arm the ESC.

delay(7000); // delay to allow the ESC to recognize the stopped signal.
}

void loop() {

int potVal = analogRead(potentiometerPin); // read input from potentiometer.
Serial.println(potVal);
int pwmVal = map(potVal,0, 1023, 1300, 1400); // maps potentiometer values to PWM value.

servo.writeMicroseconds(pwmVal); // Send signal to ESC.
}
