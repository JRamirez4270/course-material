<img title="" src="https://gitlab.com/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# Lecture 3.2 - UAS Powertrain

[TOC]

## Overview

This module will introduce the following topics:

- Types of brushless motors.

- Basics of electromagnetism

- Back EMF/generator demonstration

- ESC input signal/protocols

- PWM demonstration

## Demonstration Setup

### Back ElectroMotice Force (EMF)

This demonstration shows the signal output from one/two phases of a BLDC motor when run as a generator. 

#### Setup

**Materials:**

1. Cordless drill

2. BLDC motor

3. (2) 10 KΩ resistors

4. DSO Quad oscilloscope

**Connection:**

1. Install motor in drill chuck

2. Alligator clip of two probes to single wire on motor

3. 10 KΩ resistor connected to each of the two remaining motor wires.

4. Oscope probe connected to each of the two wires through a 10KΩ resistor.

**Oscope settings:**

- AC 0.2V on both probes

- Scan @ 50 mS

- 360 time setting

**Lesson Notes:**

1. Faster motor = higher voltage

2. Faster motor = higher frequency

3. Current induced by the inactive stator which results in a voltage drop that the ESC can measure and compare to predict the next position of the stator as it rotates. 

### PWM LED Control

This demonstration shows the output of a PWM signal visually by powering a LED.

#### Setup

**Materials:**

1. Arduino UNO or similar

2. LED

3. 10 KΩ resistor (could adjust this to demo the range more fully)

4. Hookup Wires

5. DSO Quad Oscilliscope

**Connection:**

![lecture3.2_arduinoDemo](../../img/lecture3.2_arduinoDemo.svg)

The PWM signal can be monitored with a Oscope with the following connections:

- Alligator clip of two probes to single wire on motor

- 10 KΩ resistor connected to each of the two remaining motor wires.

- Oscope probe connected to each of the two wires through a 10KΩ resistor.

**Oscope Settings:**

- DC 2V

- Scan @ 2 mS

- 4K time setting

**Code:**

Download [here](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/week3/lecture3.2/demo3.2_code/demo3.2_code.ino?inline=false)

```cpp
int pwmPin = 3;
byte pwmValue;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(pwmPin, pwmValue);
  pwmValue++;
  delay(20);
}
```

**Lesson Notes:**

This demonstration shows the voltage "chopping" that PWM is capable of. 

## Chalkboard Diagrams

### PWM Diagram

This diagram shows the parts of a PWM signal and shows how to calculate duty cycle and average voltage. This would also be a great time to discuss RMS.

![lecture3.2_pwmDiagram](../../img/lecture3.2_pwmDiagram.jpg)

[![CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
